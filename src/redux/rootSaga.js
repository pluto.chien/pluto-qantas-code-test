import { all } from "redux-saga/effects";
import { airportListWatcherSaga } from "./modules/dataFetch.saga";
import { modalWatcherSaga } from "./modules/modal.saga";

export default function* rootSaga() {
  yield all([airportListWatcherSaga(), modalWatcherSaga]);
}
