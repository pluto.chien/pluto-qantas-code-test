import { put, takeLatest } from "redux-saga/effects";

// Actions types
const MODAL_OPEN = "MODAL_OPEN";
const MODAL_CLOSE = "MODAL_CLOSE";

const initialState = { isModalOpen: false };

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case MODAL_OPEN:
      return { ...state, isModalOpen: true, airportData: action.payload };
    case MODAL_CLOSE:
      return {
        ...state,
        isModalOpen: false,
        airportData: {}
      };
    default:
      return state;
  }
}

// Action Creators
export function updateModalSaga({ isModelOpen, airportData }) {
  if (isModelOpen) {
    return { type: MODAL_OPEN, payload: airportData };
  }
  return { type: MODAL_CLOSE };
}

export function* modalSaga(value) {
  yield put(updateModalSaga(value));
}

export function* modalWatcherSaga() {
  yield takeLatest(MODAL_OPEN, modalSaga(true));
  yield takeLatest(MODAL_CLOSE, modalSaga(false));
}
