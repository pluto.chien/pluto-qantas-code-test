import axios from "axios";
import { call, put, takeLatest } from "redux-saga/effects";

// API
export const getData = () =>
  axios.get("https://api.qantas.com/flight/refData/airport");

// Actions types
const FETCH_AIRPORT_LIST = "FETCH_AIRPORT_LIST";
const FETCH_AIRPORT_LIST_SUCCESS = "FETCH_AIRPORT_LIST_SUCCESS";
const FETCH_AIRPORT_LIST_FAILURE = "FETCH_AIRPORT_LIST_FAILURE";

const initialState = { list: [], isLoading: false, error: "" };

// Reducer
export default function reducer(state = initialState, action = {}) {
  switch (action.type) {
    case FETCH_AIRPORT_LIST:
      return {
        ...state,
        isLoading: true
      };
    case FETCH_AIRPORT_LIST_SUCCESS:
      return {
        ...state,
        list: action.payload.data,
        isLoading: false
      };
    case FETCH_AIRPORT_LIST_FAILURE:
      return {
        ...state,
        error: action.payload,
        isLoading: false
      };
    default:
      return state;
  }
}

// Action Creators
export function loadDataSaga() {
  return { type: FETCH_AIRPORT_LIST };
}

export function loadDataSucceed(payload) {
  return { type: FETCH_AIRPORT_LIST_SUCCESS, payload };
}

export function loadDataFailed(payload) {
  return { type: FETCH_AIRPORT_LIST_FAILURE, payload };
}

export function* fetchDataSaga() {
  try {
    const response = yield call(getData, 0);
    yield put(loadDataSucceed(response));
  } catch (error) {
    yield put(loadDataFailed(error.message));
  }
}

export function* airportListWatcherSaga() {
  yield takeLatest(FETCH_AIRPORT_LIST, fetchDataSaga);
}
