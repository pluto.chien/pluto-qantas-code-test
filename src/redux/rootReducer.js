import { combineReducers } from "redux";
import airportData from "./modules/dataFetch.saga";
import ui from "./modules/modal.saga";

const rootReducer = combineReducers({
  airportData,
  ui
});

export default rootReducer;
