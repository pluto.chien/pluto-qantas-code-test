import React, { useState, useEffect, useCallback } from "react";
import PropTypes from "prop-types";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import { loadDataSaga } from "../redux/modules/dataFetch.saga";
import { updateModalSaga } from "../redux/modules/modal.saga";
import CardList from "../components/CardList";
import PageLoader from "../components/PageLoader";
import CardDetails from "../components/CardDetails";

const LayoutContainer = ({
                           fetchDataHandler,
                           dispatchModalHandler,
                           airportList,
                           selectedData,
                           isModalOpen,
                           isLoading,
                           error
                         }) => {
  const [currentCount, setCurrentCount] = useState(30);

  const handleScroll = useCallback(() => {
    if (
        window.innerHeight + document.documentElement.scrollTop !==
        document.documentElement.offsetHeight
    ) {
      return;
    }
    setCurrentCount(
        airportList.length > currentCount ? currentCount + 30 : currentCount
    );
  }, [airportList.length, currentCount]);

  const initFetch = useCallback(() => {
    fetchDataHandler();
  }, [fetchDataHandler]);

  useEffect(() => {
    initFetch();
  }, [initFetch]);

  useEffect(() => {
    document.body.style.overflow = isModalOpen ? "hidden" : "unset";
    window.addEventListener("scroll", handleScroll);
    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  });

  const renderComponent = () => {
    if (isLoading) return <PageLoader />;
    if (error) return <div className="error">Sorry unable to fetch data</div>;
    return (
        <div>
          <CardList
              displayList={airportList && airportList.slice(0, currentCount)}
              dispatchModal={dispatchModalHandler}
          />
          <CardDetails
              className="modal"
              isModalOpen={isModalOpen}
              dispatchModal={dispatchModalHandler}
              airportData={selectedData}
          />
        </div>
    );
  };

  return renderComponent();
};
const mapStateToProps = state => ({
  isLoading: state.airportData.isLoading,
  error: state.airportData.error,
  airportList: state.airportData.list,
  isModalOpen: state.ui.isModalOpen,
  selectedData: state.ui.airportData
});

const mapDispatchToProps = dispatch =>
    bindActionCreators(
        {
          fetchDataHandler: loadDataSaga,
          dispatchModalHandler: updateModalSaga
        },
        dispatch
    );

LayoutContainer.defaultProps = {
  selectedData: {
    airportName: "",
    country: { countryName: "" },
    region: { regionCode: "", regionName: "" }
  }
};
LayoutContainer.propTypes = {
  fetchDataHandler: PropTypes.func.isRequired,
  dispatchModalHandler: PropTypes.func.isRequired,
  airportList: PropTypes.arrayOf(PropTypes.shape({})).isRequired,
  selectedData: PropTypes.shape({}),
  isLoading: PropTypes.bool.isRequired,
  isModalOpen: PropTypes.bool.isRequired,
  error: PropTypes.string.isRequired
};

export default connect(mapStateToProps, mapDispatchToProps)(LayoutContainer);
