import React from "react";
import { Provider } from "react-redux";
import renderer from "react-test-renderer";
import configureStore from "redux-mock-store";
import ListContainer from "./LayoutContainer";
import { updateModalSaga } from "../redux/modules/modal.saga";
const mockStore = configureStore([]);

describe("ListContainer", () => {
  let store;
  let component;
  beforeEach(() => {
    store = mockStore({
      airportData: {
        list: [
          {
            airportCode: "AAA",
            internationalAirport: false,
            location: {
              aboveSeaLevel: -99999,
              latitude: 17.25,
              latitudeRadius: -0.304,
              longitude: 145.3,
              longitudeRadius: -2.5395,
              latitudeDirection: "S",
              longitudeDirection: "W"
            },
            airportName: "Anaa",
            city: {
              cityCode: "AAA",
              cityName: "Anaa",
              timeZoneName: "Pacific/Tahiti"
            },
            country: {
              countryCode: "PF",
              countryName: "French Polynesia"
            },
            region: {
              regionCode: "SP",
              regionName: "South Pacific"
            }
          },
          {
            airportCode: "AAB",
            location: {
              aboveSeaLevel: -99999,
              latitude: 26.45,
              latitudeRadius: -0.4669,
              longitude: 141,
              longitudeRadius: 2.4609,
              latitudeDirection: "S",
              longitudeDirection: "E"
            },
            airportName: "Arrabury",
            city: {
              cityCode: "AAB",
              cityName: "Arrabury",
              timeZoneName: "Australia/Brisbane"
            },
            country: {
              countryCode: "AU",
              countryName: "Australia"
            },
            region: {
              regionCode: "AU",
              regionName: "Australia"
            }
          }
        ]
      },
      ui: {
        isModalOpen: false,
        airportData: {
          airportCode: "AAB",
          location: {
            aboveSeaLevel: -99999,
            latitude: 26.45,
            latitudeRadius: -0.4669,
            longitude: 141,
            longitudeRadius: 2.4609,
            latitudeDirection: "S",
            longitudeDirection: "E"
          },
          airportName: "Arrabury",
          city: {
            cityCode: "AAB",
            cityName: "Arrabury",
            timeZoneName: "Australia/Brisbane"
          },
          country: {
            countryCode: "AU",
            countryName: "Australia"
          },
          region: {
            regionCode: "AU",
            regionName: "Australia"
          }
        }
      }
    });
    store.dispatch = jest.fn();
    component = renderer.create(
      <Provider store={store}>
        <ListContainer />
      </Provider>
    );
  });

  it("should render with given state from Redux store", () => {
    expect(component.toJSON()).toMatchSnapshot();
  });

  it('should dispatch an action on click Detail Page', () => {
    renderer.act(() => {
      component.root.findByType('button').props.onClick();
    });
    expect(store.dispatch).toHaveBeenCalledTimes(2);
    expect(store.dispatch).toHaveBeenCalledWith(
        updateModalSaga({ payload: {} })
    );
  });
});
