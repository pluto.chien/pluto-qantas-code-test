import React from "react";
import PropTypes from "prop-types";
import { Map as GoogleMap, GoogleApiWrapper, Marker } from "google-maps-react";

const mapStyles = {
  width: "90%",
  height: "90%"
};

const Map = ({ location, google }) => {
  const { longitude, latitude } = location || {};
  return (
    <GoogleMap
      google={google}
      zoom={3}
      style={mapStyles}
      initialCenter={{
        lat: latitude,
        lng: longitude
      }}
    >
      <Marker position={{ lat: latitude, lng: longitude }} />
    </GoogleMap>
  );
};

Map.propTypes = {
  location: PropTypes.shape([]).isRequired,
  google: PropTypes.shape([]).isRequired
};
export default GoogleApiWrapper({
  apiKey: "AIzaSyAN4RYf2ohKes-CFukEwUv78JMuFMhxkSg"
})(Map);
