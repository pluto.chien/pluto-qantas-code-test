import React from "react";
import { shallow, mount } from "enzyme";
import MapContainer from "./Map";

describe("MapContainer", () => {
  const defaultProps = () => ({
    location: { longitude: "111", latitude: "222" },
    google: {
      BicyclingLayer: jest.fn(),
      DirectionsStatus: { OK: "OK" }
    }
  });
  const { location, google } = defaultProps();

  it("shallow renders without crashing", () => {
    shallow(<MapContainer location={location} google={google} />);
  });

  it("render snapshot", () => {
    const wrapper = mount(
      <MapContainer location={location} google={google} />
    );
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
