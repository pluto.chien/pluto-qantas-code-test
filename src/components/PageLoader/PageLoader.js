import React from "react";
import Loader from "react-loader-spinner";
import "./PageLoader.css";

const PageLoader = () => {
  return (
    <div className="loader">
      <Loader
        type="Rings"
        color="#00BFFF"
        height={300}
        width={300}
        timeout={3000}
      />
    </div>
  );
};

export default PageLoader;
