import React from "react";
import { shallow } from "enzyme";
import PageLoader from "./PageLoader";

describe("PageLoader component", () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<PageLoader />);
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("shallow renders without crashing", () => {
    shallow(<PageLoader />);
  });

  it("render snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
