import React from "react";
import PropTypes from "prop-types";
import Map from "../Map/Map";
import "./CardDetails.css";

const cardDetails = ({ isModalOpen, dispatchModal, airportData }) => {
  const { airportCode, airportName, city, country, location } =
    airportData || {};
  const { longitude, latitude } = location || {};
  const { cityName } = city || {};
  const { countryName } = country || {};
  if (!airportCode) return null;
  return (
    <div>
      <div
        className="modal-wrapper"
        style={{
          opacity: isModalOpen ? "1" : "0",
          display: isModalOpen ? "block" : "none"
        }}
      >
        <div className="modal">
          <div className="modal-header">
            <h3>{airportName}</h3>
          </div>
          <div className="modal-body">
            <p>
              {cityName}, {countryName}
            </p>
            <p>
              Latitude: {latitude} , longitude: {longitude}
            </p>
            <div className="google-map">
              <Map location={{ latitude, longitude }} />
            </div>
          </div>
          <div className="modal-footer">
            <button
              type="button"
              className="btn-cancel"
              onClick={() => dispatchModal({ isModelOpen: false })}
            >
              BACK
            </button>
          </div>
        </div>
      </div>
    </div>
  );
};
cardDetails.defaultProps = {
  airportData: {
    airportName: "",
    country: { countryName: "" },
    region: { regionCode: "", regionName: "" }
  }
};
cardDetails.propTypes = {
  isModalOpen: PropTypes.bool.isRequired,
  dispatchModal: PropTypes.func.isRequired,
  airportData: PropTypes.shape({
    airportName: PropTypes.string,
    country: PropTypes.shape({ countryName: PropTypes.string }),
    region: PropTypes.shape({
      regionCode: PropTypes.string,
      regionName: PropTypes.string
    })
  })
};
export default cardDetails;
