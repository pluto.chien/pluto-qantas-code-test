import React from "react";
import { shallow } from "enzyme";
import CardList from "./CardList";

describe("CardList component", () => {
  let wrapper;

  const dispatchModal = jest.fn();
  const displayList = [
    {
      airportCode: "AAA",
      internationalAirport: false,
      domesticAirport: false,
      regionalAirport: false,
      onlineIndicator: false,
      eticketableAirport: false,
      location: {
        aboveSeaLevel: -99999,
        latitude: 17.25,
        latitudeRadius: -0.304,
        longitude: 145.3,
        longitudeRadius: -2.5395,
        latitudeDirection: "S",
        longitudeDirection: "W"
      },
      airportName: "Anaa",
      city: {
        cityCode: "AAA",
        cityName: "Anaa",
        timeZoneName: "Pacific/Tahiti"
      },
      country: { countryCode: "PF", countryName: "French Polynesia" },
      region: { regionCode: "SP", regionName: "South Pacific" }
    }
  ];

  beforeEach(() => {
    wrapper = shallow(
      <CardList dispatchModal={dispatchModal} displayList={displayList} />
    );
  });

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("shallow renders without crashing", () => {
    shallow(
      <CardList dispatchModal={dispatchModal} displayList={displayList} />
    );
  });

  it("render snapshot", () => {
    expect(wrapper.debug()).toMatchSnapshot();
  });
});
