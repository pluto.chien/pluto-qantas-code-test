import React from "react";
import PropTypes from "prop-types";
import Card from "../Card/Card";
import "./CardList.css";

const CardList = ({ displayList, dispatchModal }) => (
  <div id="list" className="column-list">
    {displayList.map(({ airportCode, ...rest }) => (
      <Card
        key={airportCode}
        airportData={{ airportCode, ...rest }}
        dispatchModal={dispatchModal}
      />
    ))}
  </div>
);

CardList.propTypes = {
  dispatchModal: PropTypes.func.isRequired,
  displayList: PropTypes.arrayOf(PropTypes.shape({})).isRequired
};

export default CardList;
