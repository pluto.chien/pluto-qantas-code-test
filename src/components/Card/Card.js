import React from "react";
import PropTypes from "prop-types";
import { ReactComponent as Next } from "../../resource/next.svg";
import "./Card.css";

const Card = ({ dispatchModal, airportData }) => {
  const { airportName, country, region } = airportData;
  const { countryName } = country || {};
  const { regionCode, regionName } = region || {};
  return (
    <div
      className="card-item"
      role="button"
      tabIndex={0}
      onKeyDown={() => {
        dispatchModal({ isModelOpen: true, airportData });
      }}
      onClick={() => {
        dispatchModal({ isModelOpen: true, airportData });
      }}
    >
      <div className="column-left">
        <span className={`card__tag card__tag--${regionCode}`}>
          {regionName}
        </span>
        <div className="card-header">{`${airportName} Airport`}</div>
        <div className="card-title">{countryName}</div>
      </div>
      <div className="column-right">
        <Next />
      </div>
    </div>
  );
};

Card.propTypes = {
  dispatchModal: PropTypes.func.isRequired,
  airportData: PropTypes.shape({
    airportName: PropTypes.string.isRequired,
    country: PropTypes.shape({
      countryName: PropTypes.string.isRequired
    }).isRequired,
    region: PropTypes.shape({
      regionCode: PropTypes.string.isRequired,
      regionName: PropTypes.string.isRequired
    }).isRequired
  }).isRequired
};

const comparison = (prevProps, nextProps) => {
  return prevProps.airportCode === nextProps.airportCode;
};
export default React.memo(Card, comparison);
