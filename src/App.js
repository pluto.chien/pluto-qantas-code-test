import React from "react";
import { Provider } from "react-redux";
import configureStore from "./redux/configureStore";
import "./App.css";
import LayoutContainer from "./containers/LayoutContainer";

const store = configureStore();
function App() {
  return (
    <Provider store={store}>
      <div className="container">
        <LayoutContainer />
      </div>
    </Provider>
  );
}

export default App;
